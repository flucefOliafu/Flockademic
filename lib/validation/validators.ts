/*
 * There's almost bound to exist a proper library for this, but my Google-fu failed me.
 * Feel free to investigate alternatives.
 * ~ Vincent
 */

export interface Validator<E> {
  errorDescription: string;
  id: string;
  isValid: (input: E) => boolean;
  options: {};
}

export function isValid<E>(input: E, validators: Array<Validator<E>>) {
  return violates(input, validators).length === 0;
}

export function violates<E>(input: E, validators: Array<Validator<E>>) {
  return validators.filter((validator) => !validator.isValid(input)).map((validator) => validator.id);
}

export function getMinLengthValidator<E extends string | any[]>(options = { minLength: 1 }) {
  const validator: Validator<E> = {
    errorDescription: `At least ${options.minLength} letters long`,
    id: 'minLength',
    isValid: (input: E) => input.length >= options.minLength,
    options,
  };

  return validator;
}

export function getMaxLengthValidator<E extends string | any[]>(options: { maxLength: number }) {
  const validator: Validator<E> = {
    errorDescription: `At most ${options.maxLength} letters long`,
    id: 'maxLength',
    isValid: (input: E) => input.length <= options.maxLength,
    options,
  };

  return validator;
}
