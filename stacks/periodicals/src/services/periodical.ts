import { Periodical } from '../../../../lib/interfaces/Periodical';
import { Session } from '../../../../lib/interfaces/Session';
import { Database } from '../../../../lib/lambda/middleware/withDatabase';

// This file is ignored for test coverage in the Jest configuration
// since it is merely a translation of Javascript objects to SQL queries.
export async function fetchPeriodical(
  database: Database,
  identifier: string,
  session?: Session,
): Promise<Partial<Periodical> & { identifier: string; creatorSessionId: string; }> {
  const accessClause = (session)
    // tslint:disable-next-line:no-invalid-template-strings
    ? '(p.date_published IS NOT NULL OR p.creator_session=${sessionId} OR c.creator=${accountId})'
    : 'p.date_published IS NOT NULL';

  const result: {
    creator?: string;
    creator_session: string;
    date_published?: Date;
    description?: string;
    headline?: string;
    identifier: string;
    image?: string;
    name?: string;
  } = await database.one(
      'SELECT'
        + ' p.identifier'
        + ', p.name'
        + ', p.headline'
        + ', p.description'
        + ', p.image'
        + ', p.date_published'
        + ', p.creator_session'
        + ', c.creator'
        + ' FROM periodicals p'
        + ' LEFT JOIN periodical_creators c ON c.periodical=p.uuid'
        // tslint:disable-next-line:no-invalid-template-strings
        + ' WHERE p.identifier=${identifier} AND ' + accessClause,
      {
        accountId: (session && session.account) ? session.account.identifier : undefined,
        identifier,
        sessionId: (session) ? session.identifier : undefined,
      },
    );

  return {
    creator: (result.creator) ? { identifier: result.creator } : undefined,
    creatorSessionId: result.creator_session,
    datePublished: (result.date_published) ? result.date_published.toISOString() : undefined,
    description: result.description,
    headline: result.headline,
    identifier: result.identifier,
    image: result.image,
    name: result.name,
  };
}
