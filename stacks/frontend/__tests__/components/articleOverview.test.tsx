import {
  ArticleOverview,
} from '../../src/components/articleOverview/component';
import { OrcidButton } from '../../src/components/orcidButton/component';

import { shallow } from 'enzyme';
import toJson from 'enzyme-to-json';
import * as React from 'react';

const mockProps = {
  article: {
    associatedMedia: [
      {
        contentUrl: 'arbitrary_url',
        license: 'https://creativecommons.org/licenses/by/4.0/' as 'https://creativecommons.org/licenses/by/4.0/',
        name: 'Arbitrary filename',
      },
    ],
    author: [ { identifier: 'arbitrary_account_id' } ],
    datePublished: '2002-02-14T13:37:00.000Z',
    description: 'Arbitrary description',
    identifier: 'arbitrary_id',
    isPartOf: {
      identifier: 'arbitrary_periodical_id',
      name: 'Arbitrary journal name',
    },
    name: 'Arbitrary name',
  },
  session: {
    account: { identifier: 'arbitrary_account_id' },
    identifier: 'arbitrary_session_id',
  },
  slug: 'arbitrary-slug',
  url: 'https://flockademic.com/article/arbitrary_id',
};

it('should display the article details when they have been loaded', () => {
  const mockArticle = {
    ...mockProps.article,
    datePublished: '2002-02-14T13:37:00.000Z',
    description: 'Some abstract',
    identifier: 'some_id',
    isPartOf: {
      identifier: 'some_periodical_id',
      name: 'Some journal name',
    },
    name: 'Some name',
  };

  const overview = shallow(
    <ArticleOverview {...mockProps} article={mockArticle} url="https://flockademic.com/article/some_id" />,
  );

  expect(toJson(overview)).toMatchSnapshot();
});

it('should include the Plaudit widget if a DOI is known', () => {
  const mockArticle = {
    ...mockProps.article,
    sameAs: 'https://doai.io/10.1371/journal.pbio.1002456',
  };

  const overview = shallow(
    <ArticleOverview {...mockProps} article={mockArticle} url="https://flockademic.com/article/some_id" />,
  );

  expect(overview.find('PlauditWidget[doi="10.1371/journal.pbio.1002456"]')).toExist();
});

it('should not include the Plaudit widget if no DOI is known', () => {
  const mockArticle = {
    ...mockProps.article,
    sameAs: undefined,
  };

  const overview = shallow(
    <ArticleOverview {...mockProps} article={mockArticle} url="https://flockademic.com/article/some_id" />,
  );

  expect(overview.find('PlauditWidget')).not.toExist();
});

it('should display a link to the article settings if the current user is one of the authors', () => {
  const mockArticle = {
    ...mockProps.article,
    author: [ { identifier: 'arbitrary_account_id' } ],
    identifier: 'arbitrary_id',
  };
  const mockSession = {
    ...mockProps.session,
    account: { identifier: 'arbitrary_account_id' },
  };

  const overview = shallow(
    <ArticleOverview {...mockProps} article={mockArticle} session={mockSession} />,
  );

  expect(overview.find('Link[to="/article/arbitrary_id/manage"]')).toExist();
});

it('should not display a link to the article settings if the current user is not one of the authors', () => {
  const mockArticle = {
    ...mockProps.article,
    author: [ { identifier: 'arbitrary_account_id' } ],
    identifier: 'arbitrary_id',
  };
  const mockSession = {
    ...mockProps.session,
    account: { identifier: 'arbitrary_other_account_id' },
  };

  const overview = shallow(
    <ArticleOverview {...mockProps} article={mockArticle} session={mockSession} />,
  );

  expect(overview.find('Link[to="/article/arbitrary_id/manage"]')).not.toExist();
});

it('should not display a link to the article settings if account details could not be fetched', () => {
  const sessionlessMockProps = {
    ...mockProps,
    article: {
      ...mockProps.article,
      identifier: 'arbitrary_id',
    },
    session: undefined,
  };

  const overview = shallow(<ArticleOverview {...sessionlessMockProps}/>);

  expect(overview.find('Link[to="/article/arbitrary_id/manage"]')).not.toExist();
});

// tslint:disable-next-line:max-line-length
it('should display a link to the article settings if the article is not public yet (and hence is the current user\'s)', () => {
  const mockArticle = {
    ...mockProps.article,
    datePublished: undefined,
    identifier: 'arbitrary_id',
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle}/>);

  expect(overview.find('Link[to="/article/arbitrary_id/manage"]')).toExist();
});

it('should display the journal in which an article was published', () => {
  const mockArticle = {
    ...mockProps.article,
    isPartOf: { identifier: 'some_id', name: 'Some journal name' },
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle}/>);

  expect(overview.find('Link[to="/journal/some_id"]')).toExist();
  expect(overview.find('Link[title="View other articles in: Some journal name"]')).toExist();
});

it('should not display the journal in which an article was published if none is known', () => {
  const mockArticle = {
    ...mockProps.article,
    isPartOf: undefined,
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle}/>);

  expect(overview.find('Link[to="/journal/some_id"]')).not.toExist();
  expect(overview.find('Link[title="View other articles in: Some journal name"]')).not.toExist();
});

// tslint:disable-next-line:max-line-length
it('should suggest to upload an article for external articles that have no OA version available, but are by this user', () => {
  const mockArticle = {
    ...mockProps.article,
    associatedMedia: [],
    author: [ { sameAs: 'https://orcid.org/0000-0002-4013-9889' } ],
    identifier: undefined,
  };

  const mockSession = {
    ...mockProps.session,
    account: { identifier: 'arbitrary-id', orcid: '0000-0002-4013-9889' },
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle} session={mockSession}/>);

  expect(overview.find('.message.is-danger')).toExist();
  expect(overview.find('.message.is-danger').text())
    .toMatch('This article is not yet freely available;');
});

it('should not suggest uploading an article when the article has no author', () => {
  const mockArticle = {
    ...mockProps.article,
    associatedMedia: [],
    author: undefined,
    identifier: undefined,
  };

  const mockSession = {
    ...mockProps.session,
    account: { identifier: 'arbitrary-id', orcid: '0000-0002-4013-9889' },
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle} session={mockSession}/>);

  expect(overview.find('.message.is-danger')).not.toExist();
});

it('should suggest signing in when the user is not and thus might be the author', () => {
  const mockArticle = {
    ...mockProps.article,
    associatedMedia: [],
    author: [ { sameAs: 'https://orcid.org/0000-0002-4013-9889' } ],
    identifier: undefined,
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle} session={undefined}/>);

  expect(overview.find('.message.is-danger')).toExist();
  expect(overview.find('.message.is-danger').text())
    // tslint:disable-next-line:max-line-length
    .toMatch('Unfortunately, this article is not yet freely available. Is it yours?');
  expect(overview.find(OrcidButton)).toExist();
});

it('should suggest contacting the author when no version is freely available and the user is not the author', () => {
  const mockArticle = {
    ...mockProps.article,
    associatedMedia: [],
    author: [ { sameAs: 'https://orcid.org/0000-0002-4013-9889' } ],
    identifier: undefined,
  };

  const mockSession = {
    ...mockProps.session,
    account: { identifier: 'arbitrary-account-id', orcid: 'not-0000-0002-4013-9889' },
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle} session={mockSession}/>);

  expect(overview.find('.message.is-danger')).toExist();
  expect(overview.find('.message.is-danger').text())
    // tslint:disable-next-line:max-line-length
    .toMatch('Unfortunately, this article has not been made freely available. Encourage the author(s) to share their work by joining Flockademic!');
});

it('should suggest to go to the article settings if the article does not have a name yet', () => {
  const mockArticle = {
    ...mockProps.article,
    name: undefined,
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle}/>);

  expect(overview.find('.message.is-warning')).toExist();
  expect(overview.find('.message.is-warning').text())
    .toMatch('Please configure this article\'s title and abstract');
});

it('should suggest to go to the article settings if the article does not have an abstract yet', () => {
  const mockArticle = {
    ...mockProps.article,
    description: undefined,
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle}/>);

  expect(overview.find('.message.is-warning')).toExist();
  expect(overview.find('.message.is-warning').text())
    .toMatch('Please configure this article\'s title and abstract');
});

it('should suggest to go to the article settings if the article has not been uploaded yet', () => {
  const mockArticle = {
    ...mockProps.article,
    associatedMedia: undefined,
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle}/>);

  expect(overview.find('.message.is-warning')).toExist();
  expect(overview.find('.message.is-warning').text())
    .toMatch('Please upload your article');
});

it('should suggest to go to the article settings if the article has not been submitted to the journal yet', () => {
  const mockArticle = {
    ...mockProps.article,
    datePublished: undefined,
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle}/>);

  expect(overview.find('.message.is-warning')).toExist();
  expect(overview.find('.message.is-warning').text())
    .toMatch('Your article has not been submitted to the journal yet.');
});

it('should display a download button when there\'s a PDF associated with the article', () => {
  const mockArticle = {
    ...mockProps.article,
    associatedMedia: [
      {
        contentUrl: 'some_url',
        license: 'https://creativecommons.org/licenses/by/4.0/' as 'https://creativecommons.org/licenses/by/4.0/',
        name: 'filename.pdf',
      },
    ],
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle}/>);

  expect(overview.find('[download="filename.pdf"]')).toExist();
  expect(overview.find('[to="some_url"]')).toExist();
});

// tslint:disable-next-line:max-line-length
it('should suggest adding more details (by submitting an article to Flockademic) is an external article is available but without an abstract', () => {
  const mockArticle = {
    ...mockProps.article,
    associatedMedia: [ { contentUrl: 'arbitrary-external-pdf', name: 'arbitrary_name.pdf' } ],
    author: [ { sameAs: 'https://orcid.org/0000-0002-4013-9889' } ],
    description: undefined,
    identifier: undefined,
  };

  const mockSession = {
    ...mockProps.session,
    account: { identifier: 'arbitrary-account-id', orcid: '0000-0002-4013-9889' },
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle} session={mockSession}/>);

  expect(overview.find(`[to="/articles/new/${mockProps.slug}"]`)).toExist();
});

it('should display a link to the paywall when one is known and no PDF is avaliable', () => {
  const mockArticle = {
    ...mockProps.article,
    associatedMedia: [],
    sameAs: 'https://some-download-link',
  };

  const overview = shallow(<ArticleOverview {...mockProps} article={mockArticle}/>);

  expect(overview.find('[download]')).not.toExist();
  expect(overview.find('[to="https://some-download-link"]')).toExist();
});
